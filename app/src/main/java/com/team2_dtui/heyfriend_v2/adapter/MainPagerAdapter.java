package com.team2_dtui.heyfriend_v2.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.astuetz.PagerSlidingTabStrip;
import com.team2_dtui.heyfriend_v2.R;
import com.team2_dtui.heyfriend_v2.fragment.GMapFragment;
import com.team2_dtui.heyfriend_v2.fragment.InvitationFragment;
import com.team2_dtui.heyfriend_v2.fragment.MessageFragment;
import com.team2_dtui.heyfriend_v2.fragment.SettingFragment;

/**
 * Created by huuchi207 on 14/10/2016.
 */

public class MainPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
    private int PAGE_COUNT = 4;
    private int[] tab_icon = { R.drawable.map_unpressed_icon,
            R.drawable.sms_unpressed_icon, R.drawable.chatgroup_unpressed_icon,
            R.drawable.setting_unpressed_icon};
    //    private User currentUser;
    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
//        this.currentUser= currentUser;
    }

    @Override
    public Fragment getItem(int position) {
//        if (position == 0)
//            return new ListFriendFragment();
//        else if (position==4)
//            return new SettingFragment(currentUser);
//        else if (position==3)
//            return new ListFriendFragment();
//        else
//            return new ListMessageFragment(currentUser);
        if(position==0)
            return new GMapFragment();
        else if (position==1)
            return new MessageFragment();
        else if (position==2)
            return new InvitationFragment();
        return new SettingFragment();
    }

    @Override
    public int getPageIconResId(int position) {
        return tab_icon[position];
    }


    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
