package com.team2_dtui.heyfriend_v2.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by root on 24/10/2016.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate(){
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
