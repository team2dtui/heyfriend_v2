package com.team2_dtui.heyfriend_v2.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.Language;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.model.Step;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.team2_dtui.heyfriend_v2.R;
import com.team2_dtui.heyfriend_v2.activity.LoginActivity;
import com.team2_dtui.heyfriend_v2.activity.MainActivity;
import com.team2_dtui.heyfriend_v2.common.CommonMethod;
import com.team2_dtui.heyfriend_v2.common.Constant;
import com.team2_dtui.heyfriend_v2.object.Location;
import com.team2_dtui.heyfriend_v2.object.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import de.greenrobot.event.EventBus;

/**
 * Created by Van Quyen on 10/17/2016.
 */

public class GMapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private boolean routting = false;
    private LatLng currentLocation;
    private LatLng friendLocation;
    private ImageView img_rout;
    private DatabaseReference mDatabaseReference;
    private String clientUid;
    private FirebaseAuth mAuth;
    private FirebaseUser currentFirebaseUser;
    private String currentUid;
    private UiSettings mUISettings;
    private final String SERVER_KEY = "AIzaSyCVR9oY0Ep5TD8n37wWeMKvAsWoeRaabc0";
    private String intentKey;
    private String friendName;
    private static View view;
    private ArrayList<String> directionArray = new ArrayList<String>();
    private String friendPhotoUrl;
    private String friendStatus;
    private MarkerOptions friendMarkerOption;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        intentKey = getActivity().getIntent().getStringExtra("fragment");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
        } catch (InflateException e) {
        }


        initView(view);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        currentFirebaseUser = mAuth.getCurrentUser();

        if(currentFirebaseUser != null){
            currentUid = currentFirebaseUser.getUid();
        }else{
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
            // User is signed out
        }

        if (intentKey != null) {
            clientUid = getActivity().getIntent().getStringExtra(Constant.KEY_SEND_CLIENT);
            Log.i("CLIENT======", clientUid + "");
            mDatabaseReference.child(Constant.CHILD_USERS).child(clientUid).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    friendName = user.getName();
                    friendPhotoUrl = user.getPhotoURL();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FragmentManager fm = getChildFragmentManager();
        SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.fg_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mDatabaseReference.child(Constant.CHILD_LOCATIONS).child(clientUid).removeEventListener(valueEventListenerFriendUser);
            mDatabaseReference.child(Constant.CHILD_LOCATIONS).child(currentFirebaseUser.getUid()).removeEventListener(valueEventListenerCurrentUser);
        } catch (Exception e) {
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void initView(View view) {
        img_rout = (ImageView) view.findViewById(R.id.img_fg_map_rout);
        if (intentKey != null) {
            img_rout.setVisibility(View.VISIBLE);
            img_rout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonMethod.showAnimation(v, getActivity());
                    if (routting) {
                        routting = false;
                        routing(currentLocation, friendLocation);
                    } else {
                        routting = true;
                        routing(currentLocation, friendLocation);
                    }
                }
            });
        }
    }

    private ValueEventListener valueEventListenerCurrentUser = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Location location = dataSnapshot.getValue(Location.class);
            if (location != null) {
                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                if (!routting) displayPosition(currentLocation);
            } else {
                Toast.makeText(getActivity(), "Lỗi kết nối", Toast.LENGTH_SHORT);
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private ValueEventListener valueEventListenerFriendUser = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Location location = dataSnapshot.getValue(Location.class);
            if (location != null) {
                friendLocation = new LatLng(location.getLatitude(), location.getLongitude());
                if (routting) {
                    routing(currentLocation, friendLocation);
                } else {
                    mMap.clear();
                    displayPosition(friendLocation);
                    mMap.addMarker(new MarkerOptions().position(friendLocation).title(friendName));
                }
            } else {
                Toast.makeText(getActivity(), "Lỗi kết nối", Toast.LENGTH_SHORT);
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.i("Map===", databaseError.getMessage());
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mUISettings = mMap.getUiSettings();

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mUISettings.setZoomGesturesEnabled(true);

        setMapStyle();

        mDatabaseReference.child(Constant.CHILD_LOCATIONS).child(currentUid).addValueEventListener(valueEventListenerCurrentUser);

        if (intentKey != null) {
            mDatabaseReference.child(Constant.CHILD_LOCATIONS).child(clientUid).addValueEventListener(valueEventListenerFriendUser);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(currentLocation);
                    builder.include(friendLocation);
                    LatLngBounds bounds = builder.build();
                    try {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
                    } catch (Exception e) {
                    }
                }
            }, 500);
        } else {
            displayPosition(currentLocation);
        }

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                DirectionFragment dialog = new DirectionFragment();
                dialog.show(getFragmentManager(), "ddđ");
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("direction", directionArray);
                dialog.setArguments(bundle);
            }
        });
    }


    private void displayPosition(final LatLng latLng) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng)
                            .zoom(15)
                            .build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } catch (Exception e) {
                }
            }
        }, 200);
    }

    private void setMapStyle() {
        Calendar calendar = Calendar.getInstance();
        MapStyleOptions style;

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.i("HOUR_OF_DAY===", hour + "");
        if (hour <= 5 || hour >= 19) {
            style = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.night_style_json);
        } else {
            style = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.retro_style_json);
        }
        mMap.setMapStyle(style);
    }

    public void routing(LatLng a, LatLng b) {
        if (routting && a != null && b != null) {
            GoogleDirection.withServerKey(SERVER_KEY)
                    .from(a)
                    .to(b)
                    .alternativeRoute(true)
                    .transportMode(TransportMode.DRIVING)
                    .alternativeRoute(true)
                    .language(Language.VIETNAMESE)
                    .execute(new DirectionCallback() {
                        @Override
                        public void onDirectionSuccess(Direction direction, String rawBody) {
                            String status = direction.getStatus();
                            if (status.equals(RequestResult.OK)) {
                                mMap.clear();
                                Route route = direction.getRouteList().get(0);
                                Leg leg = route.getLegList().get(0);
                                String distance = leg.getDistance().getText();
                                ArrayList<LatLng> directionPoint = leg.getDirectionPoint();
                                List<Step> list = leg.getStepList();
                                int size = list.size();
                                for (int i = 0; i < size; i++) {
                                    String html = list.get(i).getHtmlInstruction();
                                    Spanned result;
                                    result = Html.fromHtml(html);

                                    directionArray.add(String.valueOf(result));
                                }

                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                        builder.include(currentLocation);
                                        builder.include(friendLocation);
                                        LatLngBounds bounds = builder.build();
                                        try {
                                            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
                                        } catch (Exception e) {
                                        }
                                    }
                                }, 500);

                                PolylineOptions polylineOptions = DirectionConverter.createPolyline(getActivity(), directionPoint, 5, Color.RED);
                                mMap.addPolyline(polylineOptions);
                                mMap.setInfoWindowAdapter(new MyInfoWindowApdater(getActivity()));
                                Marker markerB = mMap.addMarker(new MarkerOptions().position(friendLocation)
                                        .title(friendName + "-" + distance).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
                                markerB.showInfoWindow();
                            } else {
                                Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onDirectionFailure(Throwable t) {

                        }
                    });
        }
    }

    public class MyInfoWindowApdater implements GoogleMap.InfoWindowAdapter {
        private View contentView;

        public MyInfoWindowApdater(Activity context) {
            contentView = context.getLayoutInflater().inflate(R.layout.custom_content_info, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            TextView tvName = (TextView) contentView.findViewById(R.id.tv_cci_name);
            TextView tvDistance = (TextView) contentView.findViewById(R.id.tv_cci_distance);
            tvName.setText(marker.getTitle().split("-")[0]);
            tvDistance.setText(marker.getTitle().split("-")[1]);

            return contentView;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }


}
