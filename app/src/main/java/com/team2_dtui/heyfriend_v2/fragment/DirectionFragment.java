package com.team2_dtui.heyfriend_v2.fragment;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.team2_dtui.heyfriend_v2.R;

import java.util.ArrayList;

/**
 * Created by Van Quyen on 11/9/2016.
 */

public class DirectionFragment extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.direction_info,container,false);
        ArrayList<String> directionArray = getArguments().getStringArrayList("direction");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,directionArray);
        ListView lvDirection = (ListView)rootView.findViewById(R.id.lv_di_directionlist);
        lvDirection.setAdapter(adapter);

        Button btnDismiss = (Button)rootView.findViewById(R.id.btn_di_dismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return rootView;
    }
}
