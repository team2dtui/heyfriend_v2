package com.team2_dtui.heyfriend_v2.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.team2_dtui.heyfriend_v2.common.Constant;

import de.greenrobot.event.EventBus;


/**
 * Created by Van Quyen on 10/19/2016.
 */

public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(1000)
            .setFastestInterval(500)
            .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    private Location mLastLocation;
    public static double latitude = 0;
    public static double longitude = 0;
    private FirebaseUser firebaseUser;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseReference;
    private String userUid;
    private GoogleApiClient mGoogleApiClient;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        setAuthenticatedUser(mAuth);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mGoogleApiClient.connect();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    public void setAuthenticatedUser(FirebaseAuth authData) {
        firebaseUser = authData.getCurrentUser();
        if (firebaseUser != null) {
            Log.i("AUTHDATA======",firebaseUser.getUid());
            userUid = firebaseUser.getUid();
        } else userUid = null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, REQUEST, this);
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            updateLocation(mLastLocation);
        }
    }
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    private void updateLocation(Location location){
        try{
            Log.i("LOCATION SERVICE=======",userUid+"");
            mDatabaseReference.child(Constant.CHILD_LOCATIONS).child(userUid).child(Constant.CHILD_LATITUDE).setValue(location.getLatitude());
            mDatabaseReference.child(Constant.CHILD_LOCATIONS).child(userUid).child(Constant.CHILD_LONGITUDE).setValue(location.getLongitude());
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }catch (Exception e){}
        Log.d("SERVICE======", "onLocationChanged:lam " + location.getLatitude());
    }
}

