package com.team2_dtui.heyfriend_v2.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.team2_dtui.heyfriend_v2.R;
import com.team2_dtui.heyfriend_v2.common.CommonMethod;
import com.team2_dtui.heyfriend_v2.service.LocationService;

/**
 * Created by Van Quyen on 11/1/2016.
 */

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener {

    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private TextView tvSplash;
    private Animation animation;
    FirebaseUser firebaseUser = CommonMethod.getCurrentFirebaseUser();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        tvSplash = (TextView)findViewById(R.id.splash_tv);
        Typeface font = Typeface.createFromAsset(getAssets(),"SVN_Caprica_Script.ttf");
        tvSplash.setTypeface(font);
        animation = AnimationUtils.loadAnimation(this,R.anim.fade_in);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                tvSplash.setVisibility(View.VISIBLE);
                tvSplash.startAnimation(animation);
                if (firebaseUser!= null){
                    Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                }
                else{
                    Intent mainIntent = new Intent(SplashActivity.this,LoginActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                }
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
