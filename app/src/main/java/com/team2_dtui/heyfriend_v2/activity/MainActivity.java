package com.team2_dtui.heyfriend_v2.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.team2_dtui.heyfriend_v2.ListFriendButtonAnimation;
import com.team2_dtui.heyfriend_v2.R;
import com.team2_dtui.heyfriend_v2.adapter.ListFriendAdapter;
import com.team2_dtui.heyfriend_v2.adapter.MainPagerAdapter;
import com.team2_dtui.heyfriend_v2.common.CommonMethod;
import com.team2_dtui.heyfriend_v2.common.Constant;
import com.team2_dtui.heyfriend_v2.common.Constant;
import com.team2_dtui.heyfriend_v2.dialog.UserInfoDialogFragment;
import com.team2_dtui.heyfriend_v2.object.Message;
import com.team2_dtui.heyfriend_v2.object.User;
import com.team2_dtui.heyfriend_v2.service.LocationService;
import com.team2_dtui.heyfriend_v2.service.OnStopAppService;
import com.team2_dtui.heyfriend_v2.service.ReceiveMessageService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ViewPager viewPager;
    private MainPagerAdapter mainPagerAdapter;
    private PagerSlidingTabStrip pagerSlidingTabStrip;
    private DatabaseReference mDatabaseReference;
    private FirebaseDatabase firebaseDatabase;

    private FirebaseUser currentFirebaseUser;
    private User currentUser;
    private String TAG= "MainActivity";
    private FirebaseAuth mAuth;
    public View btFriendList;

    DrawerLayout drawerLayout;
    ListView lvFriendsOnline, lvFriendsOffline;
    ListFriendAdapter friendsOnlineAdapter, friendsOfflineAdapter;
    ArrayList friendsOnline, friendsOffline;
    Map<String, Boolean> isAFriend;
    TextView tvTotalFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(MainActivity.this, OnStopAppService.class));
        startService(new Intent(MainActivity.this, LocationService.class));
        setContentView(R.layout.activity_main);
        //TODO: test action bar
//        getSupportActionBar().hide();

        initViews();

        //get current user
        mAuth = FirebaseAuth.getInstance();
        currentFirebaseUser = mAuth.getCurrentUser();
        if (currentFirebaseUser != null) {
            // User is signed in

            mDatabaseReference.child(Constant.CHILD_USERS).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            if (snapshot.hasChild(currentFirebaseUser.getUid())){
                                User user= snapshot.child(currentFirebaseUser.getUid()).getValue(User.class);
                                if (user.getName()!= null && user.getUid()!= null&&
                                        user.getDateOfBirth()!= null && user.getPhotoURL()!= null
                                        && user.getGender()!=0){
                                    startService(new Intent(MainActivity.this, ReceiveMessageService.class));
                                }
                            }
                            else {
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                finish();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(MainActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    });

        } else {
            //start login activity if user isn't signed in
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
            // User is signed out
            Log.e(TAG, "onAuthStateChanged:signed_out");
        }

        if (currentFirebaseUser!= null){
            getListFriends();
        }
        lvFriendsOnline.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Boolean b= true;
//                if (isAFriend.containsKey(friendsOnlineAdapter.getItem(position).getUid()))
//                    b= true;
                UserInfoDialogFragment dialog = new UserInfoDialogFragment(friendsOnlineAdapter.getItem(position), b);
                dialog.show(MainActivity.this.getFragmentManager(), "");
            }
        });
        lvFriendsOffline.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Boolean b= true;
//                if (isAFriend.containsKey(friendsOnlineAdapter.getItem(position).getUid()))
//                    b= true;
                UserInfoDialogFragment dialog = new UserInfoDialogFragment(friendsOfflineAdapter.getItem(position), b);
                dialog.show(MainActivity.this.getFragmentManager(), "");
            }
        });
    }
    private void initViews() {
        firebaseDatabase= FirebaseDatabase.getInstance();
        if (firebaseDatabase == null)
            firebaseDatabase.setPersistenceEnabled(true);
        mDatabaseReference = firebaseDatabase.getReference();

        btFriendList=  findViewById(R.id.btListFriend);
        final ListFriendButtonAnimation hamburgerMenuDrawable = new ListFriendButtonAnimation(getResources().getDimensionPixelSize(R.dimen.stroke_width), Color.parseColor("#ddd9e4"), Color.parseColor("#38851f"));
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            btFriendList.setBackgroundDrawable(hamburgerMenuDrawable);
        } else {
            btFriendList.setBackground(hamburgerMenuDrawable);
        }
        btFriendList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hamburgerMenuDrawable.toggle();
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        viewPager = (ViewPager) findViewById(R.id.vp_main_view_pager);
        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mainPagerAdapter);
        pagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tab_main_pager_sliding);
        pagerSlidingTabStrip.setShouldExpand(false);
        pagerSlidingTabStrip.setViewPager(viewPager);
        viewPager.setCurrentItem(0);

        lvFriendsOffline = (ListView) findViewById(R.id.list_friend_offline);
        lvFriendsOnline = (ListView) findViewById(R.id.list_friend_online);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        tvTotalFriends = (TextView) findViewById(R.id.tvTotalFriends);
        isAFriend = new HashMap<>();
        friendsOffline = new ArrayList();
        friendsOnline = new ArrayList();

        friendsOfflineAdapter = new ListFriendAdapter(this, R.layout.itemlist_friendoffline,friendsOffline);
        friendsOnlineAdapter = new ListFriendAdapter(this, R.layout.itemlist_friendonline, friendsOnline);

        lvFriendsOffline.setAdapter(friendsOfflineAdapter);
        lvFriendsOnline.setAdapter(friendsOnlineAdapter);


    }
    @Override
    public void onClick(View view) {

    }
    @Override
    public void onStart() {
        super.onStart();
        //update state of user and update last time onlineMap<String, Boolean> isAFriend;
        if (currentFirebaseUser!=null)
            CommonMethod.updateOnlineState(currentFirebaseUser, mDatabaseReference);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onResume() {
        super.onResume();
        //update state of user
        if (currentFirebaseUser!=null)
            CommonMethod.updateOnlineState(currentFirebaseUser, mDatabaseReference);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        //update state of user and update last time online
        if (currentFirebaseUser!=null)
            CommonMethod.updateOfflineState(currentFirebaseUser, mDatabaseReference);
    }

    void getListFriends(){
        mDatabaseReference.child(Constant.CHILD_USERS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (friendsOnline!= null)
                    friendsOnline.clear();
                if (friendsOffline!= null)
                    friendsOffline.clear();
                ArrayList<User> allUsers= new ArrayList<>();
                ArrayList<User> allFriends= new ArrayList<>();
                for (DataSnapshot ds: dataSnapshot.getChildren()){
                    User user= ds.getValue(User.class);
                    if (currentFirebaseUser!=null){
                        if (user.getUid().equals(currentFirebaseUser.getUid())){
                            if (isAFriend!=null)
                                isAFriend.clear();
                            if (ds.hasChild(Constant.KEY_FRIENDS)){
                                for (DataSnapshot dss : ds.child(Constant.KEY_FRIENDS).getChildren()){
                                    if (isAFriend!=null)
                                        isAFriend.put(dss.getKey().toString(), true);
                                }
                            }
                        }
                        else{
                            allUsers.add(user);
                        }
                    }
                }
                for (User user: allUsers) {
                    if (isAFriend.containsKey(user.getUid())) {
                        allFriends.add(user);
                    }
                }
                if (tvTotalFriends!=null)
                    tvTotalFriends.setText(getString(R.string.txt_total)+ allFriends.size()+" "+ getString(R.string.txt_friends));
                for (User user: allFriends){
//                    Log.e("test method", "");
                    if (user.isConnection()){
                        friendsOnline.add(user);
                        friendsOnlineAdapter.notifyDataSetChanged();
                    }
                    else{
                        friendsOffline.add(user);
                        friendsOfflineAdapter.notifyDataSetChanged();
                    }
                }
//                    if (isAFriend.containsKey(user.getUid())){
//                        friendsOnline.add(user);
//                        friendsOnlineAdapter.notifyDataSetChanged();
//                    }
//                    else{
//                        friendsOffline.add(user);
//                        friendsOfflineAdapter.notifyDataSetChanged();
//                    }
//                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    }
